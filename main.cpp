/* A very simple Hello World app */


// Qt headers
#include <QtWidgets/QApplication>
#include <QtWidgets/QPushButton>

// PN Driver standard headers
#include "pniobase.h"
#include "pniousrx.h"
#include "pnioerrx.h"
#include "servusrx.h"


using namespace std;

static PNIO_DEBUG_SETTINGS_TYPE debSet;
extern PNIO_UINT32 g_ApplHandle;

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);           // standard Qt application object

    QPushButton hello("Hello world", 0);    // widget with a pushbutton that just says hello world
    hello.resize( 100, 30 );
        
    hello.show();       // displays widget until it's closed
    return app.exec();  // exits the app when the widget is closed
}



/* NOTE: NONE of this code actually gets run. It's only here to trigger gcc to link in the
 *  profinet library when building and linking this test app */
void init()
{
  
    PNIO_UINT32 dwErrorCode = PNIO_OK;

        /* if you comment out the following line out, then rebuild the app, the assert error
         * doesn't happen. If there are no calls to a function, the compiler is smart and
         * doesn't actually link the library. The take away here is that when the PN library
         * performs its intialization it's throwing the assert error: no calls even need to be
         * made to cause the error */
    
    dwErrorCode = SERV_CP_init(&debSet);

    
}
